# Issue Tracker for DEP eReporting tool at PA Clean Water Reporting (www.pacleanwaterreporting.org)

The Department of Environmental Protection (DEP) has worked with Penn State University to develop a web-based annual reporting system for MS4s.  
A beta version of this system is complete and is available at www.pacleanwaterreporting.org.  
The Pennsylvania Clean Water Reporting System has been developed with the intention of allowing the electronic submission of multiple types of annual reports.  
The MS4 Annual Report is the first of these anticipated reports.  This is the location to add, search and follow the progress on reported bugs or new features in the MS4 application.

## View and Search Issues

1. The default view of the Issue Tracker applies a filter to show issues with status "Open"
1. This list can be sorted by clicking on any of the column headers or filtered based on a simple text search using the input .
1. To apply your own filters and text search words, select the Advanced Filter link in the upper right.  You will be brought to a new screen.

## Add a New Issue

1.  Select the "Create Issue" and you will be presented with an input form.
1.  Title: Please provide a brief title useful for summarizing and distinguishing this issue for others  (e.g.- Open Current Report button disabled )
1.  Description:  Please include the following components each time you enter a bug report or feature request.  You can cut and paste either and use as a template.
	
	**Bug Report Template**
	
	> _Browser Application/Version:_  Chrome Version 73.0.3683.86 (Official Build) (64-bit)
	> _Steps Taken:_
	>
	>	Logged In. On the Overview screen I selected Penn State University from Facility dropdown list. 
	>
	> _Result/Problem:_  The Reports tab list one item with the status "Submitted".  The "Open Current Report" button is disabled.  I cannot view or edit the report.
	>
	> _Expected Result:_  I expected to be able to click the Open Current Report button as I had previously
	>
	> _MS4 Facility:_  Penn State University MS4  
	>
	> _Security Role:_  Preparer
	
	**Feature Request Template**
	
	>_Location:_  Overview Screen
	>
	>_Feature Name:_ Surface waters map with selectable features
	>
	>_Need/Action Addressed:_  Provide a button on the surface waters tab to open a map centered on my MS4 with a tool to select surface waters for my MS4.
	>
	
1. Kind:  Select from the list
1. Priority: Select from the list
1. Attachments: If you have a screen shot of a bug or a useful example of a new feature, please upload these here.

**_Note:_** All issues are submitted anonymously so it is important to have the information from the bug report template to be able to follow the steps that presented the problem.  The Penn State development team will monitor issues reported here provide comments and the status of fixes as they are made.  This is the location to submit and follow issues related to the web application software for submitting reports.  For report content and MS4 reporting guidelines, please use the DEP contact information on the PA Clean Water Reporting site (www.pacleanwaterreporting.org).